import random
import logging
import os
import  requests
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import  IntervalTrigger
from flask import Flask, jsonify, request
from apscheduler.jobstores.base import ConflictingIdError
from slacker import Slacker
from pprint import pprint
from recurrent import RecurringEvent
from datetime import datetime

app = Flask(__name__)
logging.basicConfig()

CHAT_TOKEN = None
SLACK_TOKEN= None

beer_times = {}
r = RecurringEvent()

def beers():
    times = []
    for  d,i in beer_times.items():
        times.append("%s - %s" % (d,i))
    return 'ID - Beer Time: \n %s' % ',\n'.join(times)

@app.route('/', methods=['POST'])
def beer_time():
    try:
        assert request.form['token'] == CHAT_TOKEN
        text =  request.form['text']
        weekly=False
        if text[0:6] == "remove":
            jobid = text[6:].strip()
            sched.remove_job(jobid)
            beer_times.pop(int(jobid))
            return jsonify({"text": "Removed %s. It's not beer time anymore :/" %jobid})
        elif text[0:4] == "list":
            if len(beer_times) > 0:
                return jsonify({"text":beers()})
            else:
                return jsonify({"text": "No beer time ;_;"})
        else:
            if text[0:6] == "weekly":
                text = text[6:]
                weekly=True
                
            id = gen_id()
            date = r.parse(text)
            if not date:
                raise Exception
            save(date, request.form['channel_id'],str(id), weekly)
            text=None
            if weekly:
                text = "Scheduled weekly starting %s" % date.strftime(
                            "%I.%M %p on %A %d %B")
            else:
                text = "Scheduled for %s" % date.strftime(
                            "%I.%M %p on %A %d %B")
            beer_times[id] = date
            return jsonify(
                    {
                        "text":text
                    })
    except ConflictingIdError:
        return jsonify({"text":"That's already beer time!"})
    except Exception as e:
        # TODO: log this properly
        return jsonify({"text":"Hmm.. something went wrong. Try again. Error: %s" % e})
    

slack = Slacker(SLACK_TOKEN)

sched = BackgroundScheduler()
def gen_id():
    return random.randint(0,1000000)


def save(time, channel, id=-1, weekly=False):
    if weekly:
        sched.add_job(beer_time,IntervalTrigger(weeks=1, start_date=time), args=[channel], id=id)
    else:
        sched.add_job(beer_time,DateTrigger(time), args=[channel], id=id)

def beer_time(channel):
    slack.chat.post_message(channel, "HEY GUYS, IT'S BEER TIME! WHOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")


if __name__=="__main__":
    CHAT_TOKEN = os.environ.get('CHAT_TOKEN')
    SLACK_TOKEN = os.environ.get('SLACK_TOKEN')
    if SLACK_TOKEN and CHAT_TOKEN:
        sched.start()
        app.run(debug=True)
    else:
        error = ", ".join(['SLACK_TOKEN' if not SLACK_TOKEN else '', 'CHAT_TOKEN' if not CHAT_TOKEN else ''])
        print("Missing environment variables")
        